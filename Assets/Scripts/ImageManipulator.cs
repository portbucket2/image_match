using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageManipulator : MonoBehaviour
{
    public int currentTarget;
    public bool isToggle = false;
    private bool pressed = false;
    public int toggleBackToTarget;
    public GameObject[] buttons;

    public float speed;

    public Transform source;
    public Transform[] targets;

    private JTween jTween;
    private WinCondition winCondition;

    private int buttonToDisable = 0;
    private int buttonToEnable = 0;

    private GameManager gameManager;

    private void Awake()
    {
        jTween = GetComponent<JTween>();
        winCondition = FindObjectOfType<WinCondition>();
        gameManager = FindObjectOfType<GameManager>();

        source.transform.localPosition = targets[currentTarget].localPosition;
        source.transform.localScale = targets[currentTarget].localScale;
    }
    private void Start()
    {
        if (isToggle)
        {
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].transform.SetParent(source);
            }
        }
    }

    public void DisableButton(int idx)
    {
        buttonToDisable = idx;
    }

    public void EnableButton(int idx)
    {
        buttonToEnable = idx;
    }

    public void ButtonClicked(int target_idx)
    {
        if (jTween.enabled)
        {
            return;
        }
        else
        {
            if (isToggle)
            {
                if (pressed)
                {
                    jTween.enabled = true;
                    jTween.Initiate(source, source, targets[toggleBackToTarget], speed, null);

                    currentTarget = toggleBackToTarget;
                }
                else
                {
                    jTween.enabled = true;
                    jTween.Initiate(source, source, targets[target_idx], speed, null);

                    currentTarget = target_idx;
                }
                
                pressed = !pressed;
            }
            else
            {
                jTween.enabled = true;
                jTween.Initiate(source, source, targets[target_idx], speed, buttons[buttonToEnable]);

                currentTarget = target_idx;
    
                buttons[buttonToDisable].SetActive(false);
            }

            gameManager.ShowTut_1(false);
            Invoke("winCheckDelayed", 0.3f);
        }

    }

    private void winCheckDelayed()
    {
        winCondition.Check();
    }

    public void EnableOnClick(GameObject toEnable, GameObject toDisable)
    {
        toDisable.SetActive(false);
        toEnable.SetActive(true);
    }
}
