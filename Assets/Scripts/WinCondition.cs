using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinCondition : MonoBehaviour
{
    public List<TargetToWin> targetToWin;
    public List<BorderVanishLogic> borderVanishLogic;
    public List<ButtonVanishLogic> buttonVanishLogic;

    public GameObject[] disableOnWin;

    public bool win = false;
    private GameManager gameManager;


    private void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();

        gameManager.levelCompleteUI.SetActive(false);

        if (Stats.currentFakeLevel == 1)
        {
            gameManager.ShowTut_1(true);
        }
        else
        {
            gameManager.ShowTut_1(false);
        }
    }

    private void Start()
    {
        gameManager.LevelStarted();
    }

    public void Check()
    {
        BorderVanishCheck();
        //ButtonVanishCheck();

        win = true;

        foreach (TargetToWin item in targetToWin)
        {
            if (item.imageManipulator.currentTarget != item.target)
            {
                win = false;
            }
        }

        if (win)
        {
            Debug.LogError("WIN!");

            Invoke("DisableOnWinDelayed", 0f);
        }

        //Invoke("BorderVanishLogic", 0.5f);
    }

    public bool ButtonVanishCheck(GameObject toEnable)
    {
        bool enableAfterJTween = true;

        foreach (ButtonVanishLogic item in buttonVanishLogic)
        {
            bool conditions_matched = (item.imageManipulatorA.currentTarget == item.targetA && item.imageManipulatorB.currentTarget == item.targetB);

            if (conditions_matched)
            {
                foreach (GameObject button in item.buttonsToHide)
                {
                    button.SetActive(false);
    
                    if (button == toEnable) // THIS BUTTON IS ABOUT TO HIDE, DON'T SHOW THIS AFTER JTWEEN FINISHED.
                    {
                        enableAfterJTween = false;
                    }
                }
            }
        }

        return enableAfterJTween;
    }

    private void BorderVanishCheck()
    {
        foreach (BorderVanishLogic item in borderVanishLogic)
        {
            bool conditions_matched = (item.imageManipulatorA.currentTarget == item.targetA && item.imageManipulatorB.currentTarget == item.targetB);

            foreach (GameObject border in item.bordersToHide)
            {
                border.SetActive(!conditions_matched);
            }
        }
    }

    private void DisableOnWinDelayed()
    {
        foreach (GameObject item in disableOnWin)
        {
            item.SetActive(false);
        }
        
        gameManager.LevelCompleted();
    }
}

[System.Serializable]
public class TargetToWin
{
    public ImageManipulator imageManipulator;
    public int target;
}

[System.Serializable]
public class BorderVanishLogic
{
    public ImageManipulator imageManipulatorA;
    public int targetA;
    public ImageManipulator imageManipulatorB;
    public int targetB;
    public GameObject[] bordersToHide;
}

[System.Serializable]
public class ButtonVanishLogic
{
    public ImageManipulator imageManipulatorA;
    public int targetA;
    public ImageManipulator imageManipulatorB;
    public int targetB;
    public GameObject[] buttonsToHide;
}