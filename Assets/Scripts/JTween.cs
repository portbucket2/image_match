using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JTween : MonoBehaviour
{
    private Transform sourceTransform;
    private Transform targetTransform;
    private float speed;
    private float interpolation = 1;

    private GameObject enableOnEnd = null;
    private WinCondition winCondition;

    private void Awake()
    {
        winCondition = FindObjectOfType<WinCondition>();
    }

    public void Initiate(Transform sourceTransform, Transform start, Transform targetTransform, float speed, GameObject enableOnEnd)
    {
        this.sourceTransform = sourceTransform;
        this.sourceTransform.localPosition = start.localPosition;
        this.sourceTransform.localScale = start.localScale;

        this.targetTransform = targetTransform;

        this.speed = speed;

        interpolation = 0;

        this.enableOnEnd = enableOnEnd;
    }

    private void FixedUpdate()
    {
        sourceTransform.localPosition = Vector3.Lerp(sourceTransform.localPosition, targetTransform.localPosition, interpolation);
        sourceTransform.localScale = Vector3.Lerp(sourceTransform.localScale, targetTransform.localScale, interpolation);

        if (interpolation < 0.9f)
        {
            interpolation += Time.fixedDeltaTime * speed;
        }
        else
        {
            sourceTransform.localPosition = targetTransform.localPosition;
            sourceTransform.localScale = targetTransform.localScale;


            if (winCondition.ButtonVanishCheck(enableOnEnd))
            {
                if (enableOnEnd != null)
                {
                    if (!winCondition.win)
                    {
                        enableOnEnd.SetActive(true);
                    }
                    else
                    {
                        enableOnEnd.SetActive(false);
                    }
                }
            }

            enabled = false;
        }
    }
}
