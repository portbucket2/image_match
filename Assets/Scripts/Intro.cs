using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour
{
    public bool resetLevels;
    public bool resetUserData;

    public bool testing;
    public int testLevel = 1;

    private void Start()
    {
        //GameAnalytics.Initialize();

        if (resetLevels)
        {
            Stats.currentFakeLevel = 1;
            PlayerPrefs.SetInt("CurrentFakeLevel", Stats.currentFakeLevel);
        }
        else
        {
            Stats.currentFakeLevel = PlayerPrefs.GetInt("CurrentFakeLevel", 1);
        }

        if (testing)
        {
            Stats.currentFakeLevel = testLevel;
            PlayerPrefs.SetInt("CurrentFakeLevel", Stats.currentFakeLevel);
        }


        SceneManager.LoadScene(Stats.CalculateNextLevel(0));
    }

}
