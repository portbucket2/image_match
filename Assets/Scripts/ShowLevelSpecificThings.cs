using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowLevelSpecificThings : MonoBehaviour
{
    public int show_on_level;
    public Animator[] animators_to_enable;


    private void Awake()
    {
        if (animators_to_enable != null)
        {
            foreach (Animator animator in animators_to_enable)
            {
                animator.enabled = Stats.currentFakeLevel == show_on_level;
            }
        }
    }
}
