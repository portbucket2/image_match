using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;
using LionStudios.Suite.Analytics;

public class GameManager : MonoBehaviour
{
    public static GameManager instance { get; private set; }

    public Action OnLevelCompleteEvent;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    //public GameObject tutorial;
    //public int tut_step = 0;

    //public Vector2[] tutorialPositions;

    //public bool failed = false;
    public bool levelCompleted = false;

    public GameObject levelCompleteUI;
    public GameObject tutorialUI_1;

    //public GameObject failedUI;

    //public Material Red;
    //public Material Green;
    //public Material Blue;
    //public Material Purple;

    private void Start()
    {
        //GameAnalytics.Initialize();

        //if (Stats.currentFakeLevel < 3)
        //{
        //    tutorial.gameObject.SetActive(true);
        //}
        //if (Stats.currentFakeLevel == 2)
        //{
        //    tutorial.transform.localPosition = tutorialPositions[0];
        //}

        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "World01", "Level " + Stats.currentFakeLevel);

        LionAnalytics.GameStart();
    }

    public void LevelStarted()
    {
        LionAnalytics.LevelStart(Stats.currentFakeLevel, Stats.attemptNum);
        Debug.LogError("Starting -- Level: " + Stats.currentFakeLevel.ToString() + ", Attempt: " + Stats.attemptNum.ToString());
    }


    //public void TrainReached()
    //{
    //    trains_reached++;

    //    if (trains_reached >= total_trains)
    //    {
    //        Debug.Log("Level Complete!");
    //        levelCompleteUI.SetActive(true);
    //        levelCompleted = true;

    //        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "World01", "Level " + Stats.currentFakeLevel);

    //        //LionAnalytics.LevelComplete(Stats.currentFakeLevel, Stats.attemptNum);

    //        //Debug.LogError("Complete -- Level: " + Stats.currentFakeLevel.ToString() + ", Attempt: " + Stats.attemptNum.ToString());

    //        Stats.attemptNum = 1;
    //    }
    //}

    public void LevelCompleted()
    {
        levelCompleteUI.SetActive(true);
        levelCompleted = true;

        LionAnalytics.LevelComplete(Stats.currentFakeLevel, Stats.attemptNum);
        Debug.LogError("Completed -- Level: " + Stats.currentFakeLevel.ToString() + ", Attempt: " + Stats.attemptNum.ToString());

        OnLevelCompleteEvent?.Invoke();
    }

    public void ShowTut_1(bool show)
    {
        tutorialUI_1.SetActive(show);
    }

    public void Failed()
    {
        //failedUI.SetActive(true);
        //failed = true;

        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "World01", "Level " + Stats.currentFakeLevel);

        //LionAnalytics.LevelFail(Stats.currentFakeLevel, Stats.attemptNum);

        //Debug.LogError("Failed -- Level: " + Stats.currentFakeLevel.ToString() + ", Attempt: " + Stats.attemptNum.ToString());
    }

    public void Restart()
    {
        //LionAnalytics.LevelRestart(Stats.currentFakeLevel, Stats.attemptNum);

        //Debug.LogError("Restart -- Level: " + Stats.currentFakeLevel.ToString() + ", Attempt: " + Stats.attemptNum.ToString());

        Stats.attemptNum++;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


    public void Next()
    {
        SceneManager.LoadScene(Stats.CalculateNextLevel(1));

        PlayerPrefs.SetInt("CurrentFakeLevel", Stats.currentFakeLevel);
    }

    //public void TutorialNext()
    //{
    //    if (Stats.currentFakeLevel == 1)
    //    {
    //        tutorial.SetActive(false);
    //    }
    //    else if (Stats.currentFakeLevel == 2)
    //    {
    //        if (tut_step >= tutorialPositions.Length)
    //        {
    //            return;
    //        }


    //        if (tut_step == 0)
    //        {
    //            tut_step++;
    //            tutorial.transform.localPosition = tutorialPositions[tut_step];
    //        }
    //        else if (tut_step == 1)
    //        {
    //            tutorial.gameObject.SetActive(false);
    //            tut_step++;
    //        }
    //    }
    //}
}

