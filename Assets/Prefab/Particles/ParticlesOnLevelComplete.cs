using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesOnLevelComplete : MonoBehaviour
{
    private GameManager gameManager;

    private void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();

        gameManager.OnLevelCompleteEvent += ReceivedLevelCompleteEvent;
    }

    private void ReceivedLevelCompleteEvent()
    {
        GetComponent<ParticleSystem>().Play();
    }

    private void OnDestroy()
    {
        gameManager.OnLevelCompleteEvent -= ReceivedLevelCompleteEvent;
    }
}
